# Kaj je to
To so skupni Git repozitoriji projekta SuperiorEngineering, gostovani na platformi GitLab.

_Kaj je Git?_
Git je orodje za distribucijo ter upravljanje z verzijami datotek - z njim si lahko delimo kodo, dokumentacijo, šale, whatever, ter jih vzporedno razvijamo brez strahu, da bi naše spremembe oteževale delo drugih kolegov.

_Kaj je Gitlab?_
- Gitlab je eden izmed mnogih ponudnikov za Git strežnik (+ še mnogo ostalega, vendar to pri nas pride v upoštev kasneje)

Če trenutno vidiš le "Navodila" projekt, potem si moraš urediti dostop do ostalega. Glej naslednjo sekcijo za to.

# Kako dostopati do repozitorijev S.E.

1. Ustvari si Gitlab account
2. Pojdi na https://gitlab.com/SuperiorEngineering ter zaprosi dostop z "Request Access"
3. Na S.E. Elektronika - Campfire napiši svoj Gitlab username

    Zakaj to zahtevamo?
    - zato da ne dodajamo kar nekih ljudi
    - zato da Maintainer-ji čim prej vidimo, da si zaprosil/-a za dostop

4. Eden izmed Maintainer-jev, trenutno so to
    - Luka Kraševec (@DejTacko)
    - Mitja Nastran (@MitjaN)
    - Žiga Terbovc (@Giga-Ziga)

    te bo dodal v grupo

5. Ko se ti potrdi dostop, bi moral/-a
    - pod "Subgroups and Projects" videti vse projekte od S.E.
    - pod "Shared Projects" videti vse projekte, ki so jih uporabniki delili z S.E. skupino (trenutno je to fscanlib)

# Ok, super, zdaj imam to zrihtano. Kako zdaj uporabljam to stvar?

_Ne da se mi brat, jebi se, I'll do what the fuck I want_

Ok, za take imam [tale guide](http://rogerdudler.github.io/git-guide/)

Ne bom vas tepel, če se ne boste držali spodnjih napotkov - tepli se boste sami.

---

Za ostale pa

Prvo: mi stremimo k "Feature branch workflow" ([kaj je to](https://www.atlassian.com/git/tutorials/comparing-workflows/feature-branch-workflow) - ne rabiš zdaj brat, samo zavedaj se)

Zdaj pa kratek uvod v zabavo:

### 1. Stankaš si dol kodo

```
git clone https://gitlab.com/super_duper_repozitorij
```

### 2. Predno začneš delati na njej, si narediš svojo vejo (branch) razvoja

Takoj ko stankaš dol repozitorij, si na "master" veji. Dokler nimaš ideje kako uporabljati Git, drži svoje parklje stran od te veje.

Zato narediš svojo vejo:
```
git branch moja_super_veja_razvoja
git checkout moja_super_veja_razvoja
```

### 2. Razvijaš na njej

_...programiraš, hekaš, nič ne spiš..._

Ko si zadovoljen/-na z neko različico, in bi si jo želel/-a shraniti predno jo zjebeš, narediš commit
```
git add .
git commit -m "Super dela vse"
```
Da si jo naredil/-a, lahko preveriš z `git log`, ki ti bo izpisal nekaj v smislu
```
commit ca82a6dff817ec66f44342007202690a93763949 <--- to ti je [ID commit-a]
Author: Lepi Dasa <super@duper.si>
Date:   Mon Dec 18 21:52:11 2018 -0100

Super dela vse
```

_...programiraš, hekaš, nič ne spiš..._

Če si zajebal/-a kako stvar in bi šel/šla raje nazaj na prejšnjo verzijo, lahko
- povrneš le posamezno datoteko z `git checkout -- pot_do_datoteke`
- rečeš _fak kako je pa to prej delalo_ in začasno povrneš prejšnje stanje z `git stash`, nato pa greš nazaj na svoj zajeb z `git stash pop`
- reces _fuck everything_ in povrneš vse datoteke na prejšnjo verzijo z `git reset --hard`, vse spremembe ki niso bile commitane se ti s tem zbrišejo

_...programiraš, hekaš, nič ne spiš..._

in to ponavljas Ad infinitum.

### 3. Objaviš svoje verzije na _svojo vejo_

Zdaj ko si delal/-a `git commit`, si si delal/-a verzije lokalno, na svojem računalniku - teh nihče ne vidi dokler jih ne pošlješ na Git strežnik.

Torej, predno se boš lahko bahal/-a pred kolegi s svojo ultra mega fantastično algortimično mojstrovino, moraš narediti
```
git push -u origin moja_super_veja_razvoja
```
Zdaj bodo lahko ostali kolegi videli tvoje verzije pod "moja_super_veja_razvoja" vejo. Če jo bodo hoteli videti in
- če tvojo vejo gledajo prvič, naredijo `git fetch --all`, `git checkout --track origin/moja_super_veja_razvoja`
- če so jo že gledali, pa želijo pogledati trenutno verzijo, pa `git pull --all` ter `git checkout moja_super_veja_razvoja`
- ko bodo končali z ogledom, pa lahko grejo nazaj na svojo verzijo z `git checkout njihova_super_veja_razvoja`

Seveda, če imajo kake lokalne spremembe, ki še niso commitane, jim ne bo dovolilo narediti checkout. V tem primeru prvo `git stash`, nato pa lahko grejo nazaj na svoj zmazek z `git checkout veja_njihovega_zmazka` ter `git stash pop`.

Prav tako ni prepovedano, da tvoj kolega/kolegica commita v tvojo vejo. Samo bodi pozoren, da potem tudi ti narediš `git pull`.

### 4. Zdruziš svoje delo v master vejo

Ko si prišel do različice, za katero si zihr da **dela tako kakor mora**™, in da bo to ostalim prišlo prav, zdruziš z master vejo.

Greš na master ter stankaš dol vse morebitne nove spremembe, ki so jih naredili ostali kolegi.
```
git checkout master
git pull
```
Zdruziš svoje spremembe s svojo vejo
```
git merge -m "Združil mojo kodo, ki dela tako kakor mora™, z master vejo" moja_super_veja_razvoja
```
Preveriš, če zdaj še vedno **dela tako kakor mora**™, ter popraviš, ker itak nikoli ne dela iz prve.

Ko prideš spet do točke, ko vse skupaj živi, pa pošlješ nazaj v vesolje z
```
git push origin master
```

### 5. ???
### 6. Profit!

.. in morda v času dokončan projekt z ne preveč grdo kodo.

# Nadaljnje branje

### Kako se git uporablja

- Standard Git Workflow https://www.atlassian.com/git/tutorials/comparing-workflows

- Feature branch workflow https://www.atlassian.com/git/tutorials/comparing-workflows/feature-branch-workflow

- Uporaba https://git-scm.com/book/en/v2/Git-Branching-Basic-Branching-and-Merging

- Dobri napotki http://justinhileman.info/article/changing-history/

### Kako se git __NE__ uporablja

- https://www.reddit.com/r/ProgrammerHumor/comments/6uglkv/developer_accidentally_deleted_three_months_of/

- https://www.reddit.com/r/ProgrammerHumor/comments/43ug8t/i_fucked_up_git_so_bad_it_turned_into_guitar_hero/

- https://randyfay.com/content/avoiding-git-disasters-gory-story

- https://speakerdeck.com/bartj3/git-fuckups


### Kako odjebati stvari ko se git narobe uporablja

- https://ohshitgit.com/

- https://github.com/k88hudson/git-flight-rules

- https://git-scm.com/book/en/v2/Git-Branching-Rebasing

- https://www.reddit.com/r/programming/comments/5y82jw/some_git_tips_courtesy_of_the_cia/

- https://git.seveas.net/undoing-all-kinds-of-mistakes.html

Predstavljeno v sliki

![git-unfuck](http://justinhileman.info/article/git-pretty/git-pretty.png)

### Če bi radi vedeli kaj se dogaja sploh

- https://wildlyinaccurate.com/a-hackers-guide-to-git/

- https://www.youtube.com/watch?v=sevc6668cQ0